class Question < ApplicationRecord
  belongs_to :user
  has_many :answers, dependent: :destroy
  has_many :votes, dependent: :destroy
  has_many :question_tags, dependent: :destroy
  has_many :tags, through: :question_tags
  acts_as_votable



  def self.search(search)
    if search
      where('title LIKE ?', "%#{search}%")
    else
      []
    end
  end

end
