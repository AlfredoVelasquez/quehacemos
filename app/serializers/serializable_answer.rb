class SerializableAnswer < JSONAPI::Serializable::Resource
  type 'answers'
  attributes :body, :user
  belongs_to :user
  belongs_to :question
end