class SerializableUser < JSONAPI::Serializable::Resource
  type 'users'
  attributes :email, :first_name, :last_name
  has_many :questions
  has_many :answers
end