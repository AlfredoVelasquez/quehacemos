class SerializableQuestion < JSONAPI::Serializable::Resource
  type 'questions'
  attributes :title, :body, :user, :answers
  belongs_to :user
  has_many :answers

end