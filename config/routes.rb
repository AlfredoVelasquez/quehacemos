Rails.application.routes.draw do

  get 'votes/new'

  get 'votes/create'
 
devise_for :users
root to: "questions#index"
 get 'questions/upvote'
 get 'questions/downvote'
 get 'answers/upvote'
 get 'answers/downvote'
resources :questions do
  resources :answers do
    resources :votes
  end
end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
